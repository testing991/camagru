DROP DATABASE IF EXISTS Camagru;

CREATE DATABASE IF NOT EXISTS Camagru;

USE Camagru;

CREATE TABLE `users` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(32) DEFAULT NULL,
    `password` varchar(60) DEFAULT NULL,
    `email` text,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COLLATE utf8_general_ci;