<?php

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Camagru</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/styles.css" />
</head>
<body>

<div class="topnav">
  <a class="active" href="#home">Home</a>
  <a href="#news">News</a>
  <a href="#contact">Contact</a>
  <a href="#about">About</a>
</div>

<form action="register.php" method="POST">
    <div  class="registerform">
        <h3 class="reghead">Register</h3>
        <label class="label" for="">Username</label>
        <br>
        <input class="input" type="text" name="username">
        <br>
        <label class="label" for="">Password</label>
        <br>
        <input class="input" type="password" name="password">
        <br>
        <label class="label" for="">Confirm Password</label>
        <br>
        <input class="input" type="password" name="confirmp">
        <br>
        <label class="label" for="">Email</label>
        <br>
        <input class="input" type="email" name="email">
        <br>
        <div style="text-align:center">
        <input class="button" type="submit">
        </div>
    </div>
</form>

</body>
</html>